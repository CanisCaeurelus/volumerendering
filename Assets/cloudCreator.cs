﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class cloudCreator : MonoBehaviour
{
    public ComputeShader shader;
    public int texResolution = 128;
    ComputeBuffer textureBuffer;
    ComputeBuffer worleyTextureBuffer;
    ComputeBuffer distanceTextureBuffer;
    ComputeBuffer normalTextureBuffer;

    [Range(0.0f, 1.0f)]
    public float pointsHeight;
    [Range(0.0f, 1.0f)]
    public float pointsRangeY;
    [Range(0.0f, 1.0f)]
    public float pointsExponentialityY;

    [Range(0.0f, 1.0f)]
    public float pointsWidthX;
    [Range(0.0f, 1.0f)]
    public float pointsRangeX;
    [Range(0.0f, 1.0f)]
    public float pointsExponentialityX;

    [Range(0.0f, 1.0f)]
    public float pointsWidthZ;
    [Range(0.0f, 1.0f)]
    public float pointsRangeZ;
    [Range(0.0f, 1.0f)]
    public float pointsExponentialityZ;

    [Range(0.0f, 1.0f)]
    public float zHeight;
    [Range(0, 2)]
    public int channel = 0;

//FBM
    [Range(1, 8)]
    public int FBMoctaves;
    [Range(-1.0f, 1.0f)]
    public float FBMaddition;
    [Range(0.0f, 3.0f)]
    public float FBMmultiply = 1.0f;

//Worley
    [Range(0, 500)]
    public int worleyPointsNumber = 15;
    [Range(0.01f, 0.5f)]
    public float worleyCloudSize = 0.15f;

    Vector3[] worleyPoints;
    ComputeBuffer pointsBuffer;
    int dispatchWorleyXkernelCount;
    int dispatchWorleyYZkernelCount;

    [Range(-1.00f, 1.0f)]
    public float worleyAddition;
    [Range(0.01f, 2.5f)]
    public float worleyMultiply = 1.0f;

    //DistanceMap
    public const int randomPointsNumber = 1000;
    Vector3Int[] distanceRandomPoints;
    ComputeBuffer randomPointsBuffer;
    int distanceHandle;
    int distanceSaveHandle;

    //NormalMap
    int normalHandle;
    int normalClearHandle;


    Renderer rend;
    RenderTexture outputTexture;

    int fbmSaveHandle;

    int worleyHandle;
    int worleyClearHandle;
    int worleySaveHandle;

    int displayHandle;
    int mixHandle;


    // Start is called before the first frame update
    void Start()
    {
        outputTexture = new RenderTexture(texResolution, texResolution, 0);
        outputTexture.enableRandomWrite = true;
        outputTexture.Create();

        rend = GetComponent<Renderer>();

        InitShader();
    }

    // Update is called once per frame
    void Update()
    {
        shader.SetFloat("FBMaddition", FBMaddition);
        shader.SetFloat("FBMmultiply", FBMmultiply);
        shader.SetFloat("worleyAddition", worleyAddition);
        shader.SetFloat("worleyMultiply", worleyMultiply);
        shader.SetInt("zHeight", (int)(zHeight * (texResolution - 1)));
        shader.SetInt("FBMoctaves", FBMoctaves);
        //shader.Dispatch(fbmSaveHandle, texResolution / 8, texResolution / 8, texResolution / 8);

        shader.Dispatch(displayHandle, texResolution / 8, texResolution / 8, 1);
        // dispatch shader

        //InitWorley();
    }

    void InitShader()
    {
        InitTexture();
        fbmSaveHandle = shader.FindKernel("CSFbmSave");
        displayHandle = shader.FindKernel("CSDisplay");
        mixHandle = shader.FindKernel("CSMix");

        shader.SetInt("texResolution", texResolution); 
        shader.SetInt("squareTexRes", texResolution * texResolution);

        shader.SetInt("zHeight", (int)(zHeight * (texResolution - 1)));

        shader.SetInt("FBMoctaves", FBMoctaves);

        shader.SetInt("randomPointsNumber", randomPointsNumber);

        shader.SetBuffer(fbmSaveHandle, "StructuredTexture", textureBuffer); // To be removed
        shader.SetBuffer(displayHandle, "StructuredTexture", textureBuffer); // To be removed
        shader.SetBuffer(mixHandle, "StructuredTexture", textureBuffer); // To be removed
        shader.SetTexture(displayHandle, "Result", outputTexture); // To be removed
        shader.SetInt("channel", 0);


        //shader.Dispatch(fbmSaveHandle, texResolution / 8, texResolution / 8, texResolution / 8);
        InitWorley();
        InitDistance();
        InitNormal();

        shader.Dispatch(mixHandle, texResolution / 8, texResolution / 8, texResolution / 8);

        rend.material.SetTexture("_MainTex", outputTexture);
    }

    void InitTexture()
    {
        if (textureBuffer != null)
        {
            textureBuffer.Release();
        }
        textureBuffer = new ComputeBuffer(texResolution * texResolution * texResolution, 4 * sizeof(float));

        if (worleyTextureBuffer != null)
        {
            worleyTextureBuffer.Release();
        }
        worleyTextureBuffer = new ComputeBuffer(texResolution * texResolution * texResolution, sizeof(float));

        if (distanceTextureBuffer != null)
        {
            distanceTextureBuffer.Release();
        }
        distanceTextureBuffer = new ComputeBuffer(texResolution * texResolution * texResolution, sizeof(float));
        
        if (normalTextureBuffer != null)
        {
            normalTextureBuffer.Release();
        }
        normalTextureBuffer = new ComputeBuffer(texResolution * texResolution * texResolution, 4 * sizeof(float));
    }

    void InitDistance()
    {
        if (null != randomPointsBuffer)
        {
            randomPointsBuffer.Release();
        }
        randomPointsBuffer = new ComputeBuffer(randomPointsNumber, 3 * sizeof(int));

        distanceHandle = shader.FindKernel("CSDistanceMap");

        distanceSaveHandle = shader.FindKernel("CSDistanceMapSave");
        calculateRandomPoints();
        randomPointsBuffer.SetData(distanceRandomPoints);

        shader.SetBuffer(distanceHandle, "RandomPoints", randomPointsBuffer);
        shader.SetBuffer(distanceHandle, "StructuredTexture", textureBuffer);
        shader.SetBuffer(distanceHandle, "DistanceTexture", distanceTextureBuffer);

        shader.SetBuffer(distanceSaveHandle, "StructuredTexture", textureBuffer);
        shader.SetBuffer(distanceSaveHandle, "DistanceTexture", distanceTextureBuffer);
    }

    void InitNormal()
    {
        normalHandle = shader.FindKernel("CSNormalMap");
        normalClearHandle = shader.FindKernel("CSClearNormalMap");

        shader.SetBuffer(normalHandle, "StructuredTexture", textureBuffer);
        shader.SetBuffer(normalHandle, "NormalTexture", normalTextureBuffer);
        shader.SetBuffer(normalClearHandle, "NormalTexture", normalTextureBuffer);
    }

    void InitWorley()
    {
        worleyHandle = shader.FindKernel("CSWorley");
        worleyClearHandle = shader.FindKernel("CSWorleyClear");
        worleySaveHandle = shader.FindKernel("CSWorleySave");

        worleyPoints = new Vector3[worleyPointsNumber];
        for (int i = 0; i < worleyPointsNumber; i++)
        {
            float randomY = Random.value;
            float expPointsY = (1.0f - pointsExponentialityY) * randomY + (pointsExponentialityY * randomY * randomY);
            int randY = (int)((expPointsY * pointsRangeY + pointsHeight) * texResolution) % texResolution;

            float randomX = Random.value - 0.5f;
            float minusOne = -1f;
            if (randomX > 0)
                minusOne = 1f;
            float expPointsX = (1.0f - pointsExponentialityX) * randomX + (pointsExponentialityX * randomX * randomX * minusOne);

            int randX = (int)((expPointsX * pointsRangeX + pointsWidthX) * texResolution) % texResolution;

            
            float randomZ = Random.value - 0.5f;
            minusOne = -1f;
            if (randomZ > 0)
                minusOne = 1f;

            float expPointsZ = (1.0f - pointsExponentialityZ) * randomZ + (pointsExponentialityZ * randomZ * randomZ* minusOne);

            int randZ = (int)((expPointsZ * pointsRangeZ + pointsWidthZ) * texResolution) % texResolution;

            worleyPoints[i].x = randX;
            worleyPoints[i].y = randY;// Random.value * texResolution;
            worleyPoints[i].z = randZ;
        }

        if (pointsBuffer != null)
        {
            pointsBuffer.Release();
        }
        pointsBuffer = new ComputeBuffer(worleyPointsNumber, 3 * sizeof(float));
        pointsBuffer.SetData(worleyPoints);

        uint dispatchXCount;
        uint dispatchYCount;
        uint dispatchZCount;
        shader.GetKernelThreadGroupSizes(worleyHandle, out dispatchXCount, out dispatchYCount, out dispatchZCount);
        dispatchWorleyXkernelCount = (int)((worleyPointsNumber + (dispatchXCount - 1)) / dispatchXCount);
        dispatchWorleyYZkernelCount = (int)(((2 * (int)(worleyCloudSize * texResolution)) + (dispatchYCount - 1)) / dispatchYCount);


        

        shader.SetInt("worleyRadius", (int)(worleyCloudSize * texResolution));
        shader.SetInt("zHeight", (int)(zHeight * (texResolution - 1)));

        shader.SetFloat("invertedWorleyRadius", 1.0f / (float)(worleyCloudSize * texResolution));

        // attatch textures
        shader.SetBuffer(worleyHandle, "WorleyTexture", worleyTextureBuffer);
        shader.SetBuffer(worleyHandle, "WorleyPoints", pointsBuffer);

        shader.SetBuffer(worleyClearHandle, "WorleyTexture", worleyTextureBuffer);

        shader.SetBuffer(worleySaveHandle, "WorleyTexture", worleyTextureBuffer);
        shader.SetBuffer(worleySaveHandle, "StructuredTexture", textureBuffer);

        // dispatch shader
        shader.Dispatch(worleyClearHandle, texResolution / 8, texResolution / 8, texResolution / 8);

        shader.Dispatch(worleyHandle, dispatchWorleyXkernelCount, dispatchWorleyYZkernelCount, dispatchWorleyYZkernelCount);
        shader.SetInt("channel", 1);
        shader.Dispatch(worleySaveHandle, texResolution / 8, texResolution / 8, texResolution / 8);
    }

    [MenuItem("CreateExamples/3DTexture")]
    static public void CreateTexture3D()
    {
        cloudCreator diz = FindObjectOfType<cloudCreator>();

        // bake distance
        diz.shader.Dispatch(diz.distanceHandle, diz.texResolution / 8, diz.texResolution / 8, diz.texResolution / 8);

        diz.shader.SetInt("channel", 2);
        diz.shader.Dispatch(diz.distanceSaveHandle, diz.texResolution / 8, diz.texResolution / 8, diz.texResolution / 8);
        diz.shader.Dispatch(diz.normalClearHandle, diz.texResolution / 8, diz.texResolution / 8, diz.texResolution / 8);
        diz.shader.Dispatch(diz.normalHandle, diz.texResolution / 8, diz.texResolution / 8, diz.texResolution / 8); 


        // Configure the texture
        int size = diz.texResolution; // FindObjectOfType<cloudCreator>().texResolution;
        TextureFormat format = TextureFormat.RGBA32;
        TextureWrapMode wrapMode = TextureWrapMode.Clamp;
        Vector4[] buffer = new Vector4[diz.texResolution * diz.texResolution * diz.texResolution];
        diz.textureBuffer.GetData(buffer);

        // Create the texture and apply the configuration
        Texture3D texture = new Texture3D(size, size, size, format, false);
        texture.wrapMode = wrapMode;

        // Create a 3-dimensional array to store color data
        Color[] colors = new Color[size * size * size];
        
                // Populate the array so that the x, y, and z values of the texture will map to red, blue, and green colors
                float inverseResolution = 1.0f / (size - 1.0f);
                for (int z = 0; z < size; z++)
                {
                    int zOffset = z * size * size;
                    for (int y = 0; y < size; y++)
                    {
                        int yOffset = y * size;
                        for (int x = 0; x < size; x++)
                        {
                            colors[x + yOffset + zOffset] = buffer[x + yOffset + zOffset];
                        }
                    }
                }
        
        // Copy the color values to the texture
        texture.SetPixels(colors);

        // Apply the changes to the texture and upload the updated texture to the GPU
        texture.Apply();

        // Save the texture to your Unity Project
        
        AssetDatabase.CreateAsset(texture, "Assets/Example3DTexture.asset");

        // Generate normal texture //////////////////////////////////////////////////////////////////////////////
        // Create the texture and apply the configuration
        Texture3D normTexture = new Texture3D(size, size, size, format, false);
        normTexture.wrapMode = wrapMode;
        Vector4[] normBuffer = new Vector4[diz.texResolution * diz.texResolution * diz.texResolution];
        diz.normalTextureBuffer.GetData(normBuffer);
       
        // Create a 3-dimensional array to store color data
        Color[] normals = new Color[size * size * size];

        // Populate the array so that the x, y, and z values of the texture will map to red, blue, and green colors
        for (int z = 0; z < size; z++)
        {
            int zOffset = z * size * size;
            for (int y = 0; y < size; y++)
            {
                int yOffset = y * size;
                for (int x = 0; x < size; x++)
                {
                    normals[x + yOffset + zOffset] = normBuffer[x + yOffset + zOffset];
                }
            }
        }

        // Copy the color values to the texture
        normTexture.SetPixels(normals);

        // Apply the changes to the texture and upload the updated texture to the GPU
        normTexture.Apply();

        // Save the texture to your Unity Project

        AssetDatabase.CreateAsset(normTexture, "Assets/NormalTexture.asset");
    }

    void OnDestroy()
    {
        if (textureBuffer != null)
        {
            textureBuffer.Release();
        }
        if (worleyTextureBuffer != null)
        {
            worleyTextureBuffer.Release();
        }
        if (distanceTextureBuffer != null)
        {
            distanceTextureBuffer.Release();
        }
        if (pointsBuffer != null)
        {
            pointsBuffer.Release();
        }
        if (randomPointsBuffer != null)
        {
            randomPointsBuffer.Release();
        }
        if (normalTextureBuffer != null)
        {
            normalTextureBuffer.Release();
        }

    }

    void calculateRandomPoints()
    {
        distanceRandomPoints = new Vector3Int[randomPointsNumber]; 
        for(int i=0;i< randomPointsNumber;i++)
        {
            distanceRandomPoints[i] = new Vector3Int(Random.Range(0, texResolution), Random.Range(0, texResolution), Random.Range(0, texResolution));
        }
	}
}
