﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CombinedNoiseGenerator : MonoBehaviour
{
    public Texture2D perlinNoise;
    public Texture2D worleyNoise;
    public Texture2D combinedNoise;
    public bool textureUpdated = true;

    private int _textureResolution=128;
    
    // Start is called before the first frame update
    void Start()
    {
        combinedNoise = new Texture2D(_textureResolution, _textureResolution);
    }

    // Update is called once per frame
    void Update()
    {
        if (textureUpdated)
        {
            
            for (int cordX = 0; cordX < _textureResolution; cordX++)
                for (int cordY = 0; cordY < _textureResolution; cordY++)
                {
                    float brightnessLevel = - perlinNoise.GetPixel(cordX, cordY).grayscale + worleyNoise.GetPixel(cordX, cordY).grayscale;
                    combinedNoise.SetPixel(cordX, cordY, new Color(brightnessLevel, brightnessLevel, brightnessLevel));
                }
            combinedNoise.Apply();
            GetComponent<RawImage>().texture = combinedNoise;
            textureUpdated = false;
        }
    }
}
