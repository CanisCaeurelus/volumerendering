﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorleyWithCS : MonoBehaviour
{
    public ComputeShader shader;
    public RenderTexture texture;

    public int texResolution = 256;
    private int _oldTexResolution;

    [Range(1, 500)]
    public int worleyPointsNumber = 10;
    private int _oldWorleyPointsNumber;

    

    [Range(0.01f, 0.5f)]
    public float _cloudSize = 0.15f;
    private float _oldCloudSize;

    [Range(0.0f, 1.0f)]
    public float zLevel = 0;
    private float oldZLevel;

    public bool _invert = false;
    private bool _oldInvert;

    bool paramUpdated = true;
    bool zLevelUpdated = false;

    int kernelHandle;
    int kernelNormHandle;
    int kernelClearHandle;

    int dispatchXkernelCount;
    int dispatchYZkernelCount;

    Vector3[] worleyPoints;
    ComputeBuffer pointsBuffer;

    Vector4[,] rawTextureData;
    ComputeBuffer textureBuffer;

    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        updateParams();
        if (paramUpdated)
        {
            UpdateShader();
            paramUpdated = false;
		}
        UpdateZLevel();
    }

    void OnDestroy()
    {
        pointsBuffer.Release();
        textureBuffer.Release();

    }

    private void UpdateShader()
    {   
        if(pointsBuffer!=null)
            pointsBuffer.Release();
        if (textureBuffer != null)
            textureBuffer.Release();
        texture = new RenderTexture(texResolution, texResolution, 0);
        texture.enableRandomWrite = true;
        texture.Create();

        GetComponent<RawImage>().texture = texture;

        rawTextureData = new Vector4[texResolution, texResolution];
        textureBuffer = new ComputeBuffer(texResolution * texResolution * texResolution, 4 * sizeof(float));


        worleyPoints = new Vector3[worleyPointsNumber];
        for (int i = 0; i < worleyPointsNumber; i++)
        {
            worleyPoints[i].x = Random.value * texResolution;
            worleyPoints[i].y = Random.value * texResolution;
            worleyPoints[i].z = Random.value * texResolution;
        }

        pointsBuffer = new ComputeBuffer(worleyPointsNumber, 3 * sizeof(float));
        pointsBuffer.SetData(worleyPoints);

        kernelHandle = shader.FindKernel("CSMain");
        kernelNormHandle = shader.FindKernel("CSNorm");
        kernelClearHandle = shader.FindKernel("CSClear");
        uint dispatchXCount;
        uint dispatchYCount;
        uint dispatchZCount;

        shader.GetKernelThreadGroupSizes(kernelHandle, out dispatchXCount, out dispatchYCount, out dispatchZCount);
        dispatchXkernelCount = (int)((worleyPointsNumber + (dispatchXCount - 1)) / dispatchXCount);
        dispatchYZkernelCount = (int)(((2 * (int)(_cloudSize * texResolution)) + (dispatchYCount - 1)) / dispatchYCount);
        shader.SetTexture(kernelHandle, "Result", texture);
        shader.SetBuffer(kernelHandle, "WorleyPoints", pointsBuffer);
        shader.SetBuffer(kernelHandle, "StructuredTexture", textureBuffer);
        shader.SetInt("texResolution", texResolution);
        shader.SetInt("radius", (int)(_cloudSize*texResolution));
        shader.SetInt("zLevel", (int)(zLevel * (texResolution-1)));

        shader.SetFloat("invertedRadius", 1.0f / (float)(_cloudSize * texResolution));

        shader.SetTexture(kernelNormHandle, "Result", texture);
        shader.SetBuffer(kernelNormHandle, "StructuredTexture", textureBuffer);
        shader.SetTexture(kernelClearHandle, "Result", texture);
        shader.SetBuffer(kernelClearHandle, "StructuredTexture", textureBuffer);


        shader.Dispatch(kernelClearHandle, texResolution / 8, texResolution / 8, texResolution / 8);
        shader.Dispatch(kernelHandle, dispatchXkernelCount, dispatchYZkernelCount, dispatchYZkernelCount);
        shader.Dispatch(kernelNormHandle, texResolution / 8, texResolution / 8, 1);
    }
    private void updateParams()
    {
        if ((worleyPointsNumber != _oldWorleyPointsNumber) || (texResolution != _oldTexResolution) || (_cloudSize != _oldCloudSize) || (_invert != _oldInvert))
        {
            paramUpdated = true;
        }
        _oldWorleyPointsNumber = worleyPointsNumber;
        _oldTexResolution = texResolution;
        _oldCloudSize = _cloudSize;
        _oldInvert = _invert;
        if (zLevel != oldZLevel)
        {
            zLevelUpdated = true;
		}
        oldZLevel = zLevel;
    }

    void UpdateZLevel()
    {
        if (zLevelUpdated == true)
        {
            zLevelUpdated = false;
            shader.SetInt("zLevel", (int)(zLevel * (texResolution - 1)));
            shader.Dispatch(kernelNormHandle, texResolution / 8, texResolution / 8, 1);
        }

    }

}
