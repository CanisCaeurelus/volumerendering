﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PerlinNoiseGenerator : MonoBehaviour
{
    public Texture2D perlinNoise;

    private bool paramUpdated = true;

    public int _textureResolution = 128;
    private int _oldTextureResolution;

    [Range(1, 10)]
    public int _octaves = 4;
    private int _oldOctaves;

    public bool _invert = false;
    private bool _oldInvert;

    public float _scale = 1.0f;
    private float _oldScale;

    [Range(0f, 1f)]
    public float _limitUp = 1.0f;
    private float _oldLimitUp;

    [Range(0f, 1f)]
    public float _limitLow = 0.0f;
    private float _oldLimitLow;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        updateParams();
        if (paramUpdated)
        {
            perlinNoise = new Texture2D(_textureResolution, _textureResolution);
            calculateTexture();
            GetComponent<RawImage>().texture = perlinNoise;
            paramUpdated = false ;
        }
    }

    private float calculateTexture()
    {
        float invertedScale = 1f / _scale;
        for (int cordX = 0; cordX < _textureResolution; cordX++)
            for (int cordY = 0; cordY < _textureResolution; cordY++)
            {
                float brightnessLevel = .5f* (1f+(float)Perlin.Fbm(invertedScale * 10f * (float)cordX / (float)_textureResolution, invertedScale * 10f * (float)cordY / (float)_textureResolution, _octaves)) ;
                brightnessLevel = (Mathf.Clamp(brightnessLevel, _limitLow, _limitUp) - _limitLow) / (_limitUp - _limitLow);
                brightnessLevel = _invert? (-1) * brightnessLevel + 1 : brightnessLevel;
                perlinNoise.SetPixel(cordX, cordY, new Color(brightnessLevel, brightnessLevel, brightnessLevel));
            }
        perlinNoise.Apply();
        GameObject.FindWithTag("Combined").GetComponent<CombinedNoiseGenerator>().perlinNoise = perlinNoise;
        GameObject.FindWithTag("Combined").GetComponent<CombinedNoiseGenerator>().textureUpdated = true;
        return 0f;
    }

    private void updateParams()
    {
        if ((_octaves != _oldOctaves) 
            || (_textureResolution != _oldTextureResolution) 
            || (_invert != _oldInvert) 
            || (_scale != _oldScale)
            || (_limitUp != _oldLimitUp)
            || (_limitLow != _oldLimitLow))
        {
            paramUpdated = true;
        }
        _oldOctaves = _octaves;
        _oldTextureResolution = _textureResolution;
        _oldInvert = _invert;
        _oldScale = _scale;
        _oldLimitUp = _limitUp;
        _oldLimitLow = _limitLow;
    }
}
