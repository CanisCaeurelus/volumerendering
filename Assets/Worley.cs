﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;


class Worley
{
    public List<Vector2> WorleyPoints;
    private int _worleyPointsNumber = 15;
    private float _textureResolution = 10.0f;
    private float _cloudSize = 50.0f;
    private float _cloudSizeInverted = 50.0f;

    public Worley(int textureResolution, int worleyPointsNumber, float cloudSize)
    {
        _worleyPointsNumber = worleyPointsNumber;
        _textureResolution = (float)textureResolution;
        _cloudSize = cloudSize;
        _cloudSizeInverted = 1f / cloudSize;
        WorleyPoints = GenerateWorleyPoints();
    }

    public Worley(int worleyPointsNumber, float cloudSize)
    {
        _worleyPointsNumber = worleyPointsNumber;
        _textureResolution = 10.0f;
        _cloudSize = cloudSize;
        _cloudSizeInverted = 1f / cloudSize;
        WorleyPoints = GenerateWorleyPoints();
    }
    public Worley()
    {
        _worleyPointsNumber = (int)UnityEngine.Random.Range(0f, 125f);
        _textureResolution = 10.0f;
        _cloudSize = UnityEngine.Random.Range(0.01f, 5f);
        _cloudSizeInverted = 1f / _cloudSize;
        WorleyPoints = GenerateWorleyPoints();
    }

    private List<Vector2> GenerateWorleyPoints()
    {
        List<Vector2> NewWorleyPoints = new List<Vector2>();
        for (int i = 0; i < _worleyPointsNumber; i++)
        {
            NewWorleyPoints.Add(new Vector2(UnityEngine.Random.Range(0f, _textureResolution), UnityEngine.Random.Range(0f, _textureResolution)));
            NewWorleyPoints.Add(new Vector2(NewWorleyPoints[i * 9].x + _textureResolution, NewWorleyPoints[i * 9].y));
            NewWorleyPoints.Add(new Vector2(NewWorleyPoints[i * 9].x + _textureResolution, NewWorleyPoints[i * 9].y + _textureResolution));
            NewWorleyPoints.Add(new Vector2(NewWorleyPoints[i * 9].x + _textureResolution, NewWorleyPoints[i * 9].y - _textureResolution));
            NewWorleyPoints.Add(new Vector2(NewWorleyPoints[i * 9].x - _textureResolution, NewWorleyPoints[i * 9].y));
            NewWorleyPoints.Add(new Vector2(NewWorleyPoints[i * 9].x - _textureResolution, NewWorleyPoints[i * 9].y + _textureResolution));
            NewWorleyPoints.Add(new Vector2(NewWorleyPoints[i * 9].x - _textureResolution, NewWorleyPoints[i * 9].y - _textureResolution));
            NewWorleyPoints.Add(new Vector2(NewWorleyPoints[i * 9].x, NewWorleyPoints[i * 9].y + _textureResolution));
            NewWorleyPoints.Add(new Vector2(NewWorleyPoints[i * 9].x, NewWorleyPoints[i * 9].y - _textureResolution));
        }
        return NewWorleyPoints;
    }

    private float calculateDistance(float pointX, float pointY)
    {
        float squareDistance = _textureResolution * _textureResolution * 2;
        for (int pointIter = 0; pointIter < _worleyPointsNumber * 9; pointIter++)
        {
            squareDistance = System.Math.Min(Mathf.Pow(WorleyPoints[pointIter].x- pointX, 2) + Mathf.Pow(WorleyPoints[pointIter].y - pointY, 2), squareDistance);
        }
        return Mathf.Sqrt(squareDistance);
    }

    public float Noise(float cordX, float cordY)
    {
        float distance = calculateDistance(cordX, cordY);
        return 1.0f - Mathf.Clamp(distance * _cloudSizeInverted, 0.0f, 1.0f);
    }
}

