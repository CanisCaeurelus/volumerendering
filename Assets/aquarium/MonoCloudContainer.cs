﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class MonoCloudContainer : MonoBehaviour
{
    
    private Material material=null;
    public Shader shader;
    public Texture3D cloudTexture;
    public float cloudScale = 1f;
    Vector3 oldPosition;
    Vector3 oldLossyScale;

    private int boundsMinID = 0;
    private int boundsMaxID = 0;
    private int cloudsResolutionID = 0;
    private int textureLengthID = 0;
    private int invertedTextureLengthID = 0;
    private int timeID = 0;

    // Start is called before the first frame update
    void Start()
    {
        if(null == material)
        {
            material = new Material(shader);
		}
        GetComponent<Renderer>().material = material;
        updateContainerTransformation();
        oldPosition = transform.position;
        oldLossyScale = transform.lossyScale;
    }

    // Update is called once per frame
    void Update()
    {
        if (null == material)
        {
            material = new Material(shader);
            GetComponent<Renderer>().material = material;
        }
        if ((oldPosition != transform.position) || (oldLossyScale != transform.lossyScale))
        {
            updateContainerTransformation();
            oldPosition = transform.position;
            oldLossyScale = transform.lossyScale;
        }
        material.SetFloat(timeID, Time.time);
        material.SetFloat(cloudsResolutionID, 1 / cloudScale);
    }

    void updateContainerTransformation()
    {
        Vector3 boundsMin;
        Vector3 boundsMax;
        boundsMin = transform.position - transform.lossyScale * 0.5f;
        boundsMax = transform.position + transform.lossyScale * 0.5f;

        boundsMinID = Shader.PropertyToID("_boundsMin");
        boundsMaxID = Shader.PropertyToID("_boundsMax");
        cloudsResolutionID = Shader.PropertyToID("_cloudRes");
        textureLengthID = Shader.PropertyToID("_textureLength");
        invertedTextureLengthID = Shader.PropertyToID("_invertedTextureLength");
        timeID = Shader.PropertyToID("_time");

        material.SetVector(boundsMinID, boundsMin);
        material.SetVector(boundsMaxID, boundsMax);
        material.SetFloat(textureLengthID, (float)cloudTexture.height);
        material.SetTexture("_CloudTex", cloudTexture);
    }
}
