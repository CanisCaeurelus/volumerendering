﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class MonoAquarium : MonoBehaviour
{
    public ComputeShader computeShader;
    public int texResolution = 128;

    private Texture3D texture;
    private Material material=null;
    public Shader shader;
    public Texture3D cloudTexture;
    public float cloudScale = 1f;
    Vector3 oldPosition;
    Vector3 oldLossyScale;

    private int centerID = 0;
    private int radiusID = 0;
    private int cloudsResolutionID = 0;
    private int textureLengthID = 0;
    private int invertedTextureLengthID = 0;
    private int timeID = 0;

    // Start is called before the first frame update
    void Start()
    {



        if (null == material)
        {
            material = new Material(shader);
		}
        GetComponent<Renderer>().material = material;
        updateContainerTransformation();
        oldPosition = transform.position;
        oldLossyScale = transform.lossyScale;
    }

    // Update is called once per frame
    void Update()
    {
        if (null == material)
        {
            material = new Material(shader);
            GetComponent<Renderer>().material = material;
        }
        if ((oldPosition != transform.position) || (oldLossyScale != transform.lossyScale))
        {
            updateContainerTransformation();
            oldPosition = transform.position;
            oldLossyScale = transform.lossyScale;
        }
        material.SetFloat(timeID, Time.time);
        material.SetFloat(cloudsResolutionID, 1 / cloudScale);
    }

    void initTexture()
    {
        TextureFormat format = TextureFormat.RGBA32;
        TextureWrapMode wrapMode = TextureWrapMode.Clamp;
        texture = new Texture3D(texResolution, texResolution, texResolution, format, false);
        texture.wrapMode = wrapMode;
    }

    void initComputeShader()
    {

	}
    void updateContainerTransformation()
    {
        Vector3 center;
        float radius;
        center = transform.position;
        radius = transform.lossyScale.x * 0.5f;

        centerID = Shader.PropertyToID("_center");
        radiusID = Shader.PropertyToID("_radius");
        cloudsResolutionID = Shader.PropertyToID("_cloudRes");
        textureLengthID = Shader.PropertyToID("_textureLength");
        invertedTextureLengthID = Shader.PropertyToID("_invertedTextureLength");
        timeID = Shader.PropertyToID("_time");

        material.SetVector(centerID, center);
        material.SetFloat(radiusID, radius);
        material.SetFloat(textureLengthID, (float)cloudTexture.height);
        material.SetTexture("_CloudTex", cloudTexture);
    }
}
