﻿Shader "Custom/CloudShader"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _CloudTex("Albedo (RGB)", 3D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" "Queue" = "Transparent"}
        LOD 200
        
        Cull Off
        Blend SrcAlpha OneMinusSrcAlpha

        Pass {

        CGPROGRAM

        #include "UnityCG.cginc"
        // Physically based Standard lighting model, and enable shadows on all light types
        //#pragma surface surf Standard fullforwardshadows
        #pragma fragment frag
        #pragma vertex vert

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        #define SQRT_3 1.732050
        #define MAX_STEPS 80
        #define STEP_SIZE (0.0125 * SQRT_3)


        sampler2D _MainTex;
        sampler3D _CloudTex;
        float3 _boundsMin;
        float3 _boundsMax;
        float _cloudRes;
        float _textureLength;
        float _invertedTextureLength;
        float _time;
        
        float _edgeFadeDst;

        // vertex input position, uv
        struct appdata
        {
            float4 vertex : POSITION;
            float3 uv : TEXCOORD0;
        };

        struct v2f
        {
            float4 pos : SV_POSITION;
            float3 uv : TEXCOORD0;
            float3 ro : TEXCOORD1;
            float3 hitPos : TEXCOORD2;
        };

        struct Input
        {
            float2 uv_MainTex;
            float3 uv_CloudTex;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;

        bool insideCube(float3 boundsMin, float3 boundsMax, float3 p)
        {
            return (boundsMin.y <= p.y && boundsMin.z <= p.z && boundsMin.x <= p.x
                && boundsMax.y >= p.y && boundsMax.z >= p.z && boundsMax.x >= p.x);
        }

        v2f vert(appdata v)
        {
            v2f output;
            output.pos = UnityObjectToClipPos(v.vertex);
            output.uv = v.uv;
            //output.ro = mul(unity_WorldToObject, float4(_WorldSpaceCameraPos, 1));
            output.ro = _WorldSpaceCameraPos;

            output.hitPos = mul(unity_ObjectToWorld, v.vertex);
            
            
            return output;
        }

        float fadeEdges(float3 p, float fadeDst)
        {
            float dstFromEdgeX = min(fadeDst, min(abs(p.x-_boundsMin.x), abs(_boundsMax.x-p.x)));
            float dstFromEdgeZ = min(fadeDst, min(abs(p.z - _boundsMin.z), abs(_boundsMax.z - p.z)));
            float dstFromEdgeY = min(fadeDst, min(abs(p.y - _boundsMin.y), abs(_boundsMax.y - p.y)));

            return clamp((min(min(dstFromEdgeZ, dstFromEdgeX), dstFromEdgeY) ) / fadeDst,0,1);
        }

        float3 getTexturePoint(float3 p)
        {
            float3 timeOffset = float3(0.8 * _time, 0.0, 0.1 * _time);
            return frac((p + timeOffset) * _cloudRes);
        }

        float shape(float3 p)
        {
            float3 texturePoint = getTexturePoint(p);
            
            return tex3D(_CloudTex, texturePoint).w * fadeEdges(p,0.4);
            //return length(p) - .5;
        }

        float4 rayMarch(float3 ro, float3 rd, float3 boundsMin, float3 boundsMax)
        {
            float retVal=0;
            float dO = 0;
            float dS=0;
            float3 p = ro;
            float3 retPoint= float3(0,0,0);
            float3 nextStep;
            
            float3 texturePoint = getTexturePoint(p);
            float stepLength;
            for (int i = 0; i < MAX_STEPS; i++)
            {
                stepLength = tex3D(_CloudTex, texturePoint).z * _cloudRes * _invertedTextureLength;
                stepLength = max(stepLength, STEP_SIZE);
                nextStep = rd * stepLength;
                p += nextStep;
                dS += 0.08 * clamp(shape(p)-0.05,0,1);
                if (dS > 0.17 && dO == 0)
                {
                    dO = 1;
                    retPoint = p;
                }
                if (dS > 0.95 ) {  break; }
                if (!insideCube(boundsMin, boundsMax,p)) 
                { break; }
                texturePoint = getTexturePoint(p);
                
            }
            retVal = dS;
            return float4(retPoint,retVal);
        }

        float lightMarch(float3 ro, float3 rd, float3 boundsMin, float3 boundsMax)
        {
            float retVal = 0;
            float dO = 0;
            float dS = 0;
            float3 p = ro;
            float3 nextStep;

            float3 texturePoint = getTexturePoint(p);
            float stepLength;
            for (int i = 0; i < MAX_STEPS; i++)
            {
                stepLength = tex3D(_CloudTex, texturePoint).z * _cloudRes * _invertedTextureLength;
                stepLength = max(stepLength, STEP_SIZE);
                nextStep = rd * stepLength;
                p += nextStep;
                dS += 0.08 * clamp(shape(p) - 0.05, 0, 1);
                if (dS > 0.95) { break; }
                if (!insideCube(boundsMin, boundsMax, p))
                {
                    break;
                }
                texturePoint = getTexturePoint(p);

            }
            retVal = dS;
            return retVal;
        }
     
        float4 frag(v2f i) : SV_Target
        {
            float3 uv = i.uv;
            float3 ro = i.ro;
            float3 rd = normalize(i.hitPos - ro);
            
            if (!insideCube(_boundsMin, _boundsMax, ro))
            {
                ro = i.hitPos;
            }

            float3 rayPos = _WorldSpaceCameraPos;
            
            float4 result = rayMarch(ro, rd, _boundsMin, _boundsMax);
            //float lighting = lightMarch(result.xyz, _WorldSpaceLightPos0, _boundsMin, _boundsMax);
            
            float4 col = float4(1.0, 1.0, 1.0, result.w);
            float3 wallColor = float3((1.0 + cos(i.hitPos.y * 0.01 + 3.14)) * 0.5,
                (1.0 + sin(i.hitPos.y * 0.05 + 4.7)) * 0.5,
                (1.0 + cos(i.hitPos.y * 0.02 + 1.7)) * 0.5);
            col.xyz = lerp(wallColor, float3(1.0, 1.0, 1.0), result.w);
                col.w = result.w;
            return col;
        }

        ENDCG
        }
    }
    FallBack "Diffuse"
}
