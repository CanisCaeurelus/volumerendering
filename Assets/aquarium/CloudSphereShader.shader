﻿Shader "Custom/CloudSphereShader"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _CloudTex("Albedo (RGB)", 3D) = "white" {}
        _NormalTex("Normal", 3D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" "Queue" = "Transparent"}
        LOD 200
        
        Cull Off
        Blend SrcAlpha OneMinusSrcAlpha

        Pass {

        CGPROGRAM

        #include "UnityCG.cginc"
        // Physically based Standard lighting model, and enable shadows on all light types
        //#pragma surface surf Standard fullforwardshadows
        #pragma fragment frag
        #pragma vertex vert

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        #define SQRT_3 1.732050
        #define MAX_STEPS 80
        #define STEP_SIZE (0.0125 * SQRT_3)


        sampler2D _MainTex;
        sampler3D _CloudTex;
        sampler3D _NormalTex;
        float3 _center;
        float _radius;
        float _cloudRes;
        float _textureLength;
        float _invertedTextureLength;
        float _time;
        fixed4 _LightColor0;
        
        float _edgeFadeDst;

        // vertex input position, uv
        struct appdata
        {
            float4 vertex : POSITION;
            float3 uv : TEXCOORD0;
        };

        struct v2f
        {
            float4 pos : SV_POSITION;
            float3 uv : TEXCOORD0;
            float3 ro : TEXCOORD1;
            float3 hitPos : TEXCOORD2;
        };

        struct Input
        {
            float2 uv_MainTex;
            float3 uv_CloudTex;
            float3 uv_NormalTex;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;

        bool insideSphere(float3 center, float radius, float3 p)
        {
            return distance(p,center) <= radius;
        }

        v2f vert(appdata v)
        {
            v2f output;
            output.pos = UnityObjectToClipPos(v.vertex);
            output.uv = v.uv;
            //output.ro = mul(unity_WorldToObject, float4(_WorldSpaceCameraPos, 1));
            output.ro = _WorldSpaceCameraPos;

            output.hitPos = mul(unity_ObjectToWorld, v.vertex);
            
            
            return output;
        }

        float fadeEdges(float3 p, float fadeDst)
        {
            float dstFromEdge = _radius - distance(p,_center);

            return clamp(dstFromEdge / fadeDst,0,1);
        }

        float3 getTexturePoint(float3 p)
        {
            float3 timeOffset = float3(0.1 * _time, 0.0, 0.1 * _time);
            return frac((p + timeOffset) * _cloudRes);
        }

        float shape(float3 p)
        {
            float3 texturePoint = getTexturePoint(p);
            
            return tex3D(_CloudTex, texturePoint).w * fadeEdges(p,0.4);
            //return length(p) - .5;
        }

        float4 rayMarch(float3 ro, float3 rd, float3 center, float radius)
        {
            float retVal=0;
            float dO = 0;
            float dS=0;
            float3 p = ro;
            float3 retPoint= float3(0,0,0);
            float3 nextStep;
            float shadeGeomSum = 0;
            
            float3 texturePoint = getTexturePoint(p);
            float stepLength;
            for (int i = 0; i < MAX_STEPS; i++)
            {
                stepLength = tex3D(_CloudTex, texturePoint).z * _cloudRes * _invertedTextureLength;
                stepLength = max(stepLength, STEP_SIZE);
                nextStep = rd * stepLength;
                p += nextStep;
                float pointDensity = 0.2 * clamp(shape(p) - 0.05, 0, 1);
                dS += pointDensity;
                shadeGeomSum += 5*pointDensity * dot(_WorldSpaceLightPos0.xyz,tex3D(_NormalTex, texturePoint).xyz);

                if (dS > 0.17 && dO == 0)
                {
                    dO = 1;
                    retPoint = p;
                }
                if (dS > 0.95 ) {  break; }
                if (!insideSphere(center, radius,p))
                { break; }
                texturePoint = getTexturePoint(p);
                
            }
            retVal = dS;
            if (dS > 0.01)
            {
                shadeGeomSum /= (2.5*dS);
            }
            //return float4(tex3D(_NormalTex, texturePoint).xyz, retVal);
            return float4(shadeGeomSum, shadeGeomSum, shadeGeomSum,retVal);
        }

        float lightMarch(float3 ro, float3 rd, float3 center, float radius)
        {
            float retVal = 0;
            float dO = 0;
            float dS = 0;
            float3 p = ro;
            float3 nextStep;

            float3 texturePoint = getTexturePoint(p);
            float stepLength;
            for (int i = 0; i < MAX_STEPS; i++)
            {
                stepLength = tex3D(_CloudTex, texturePoint).z * _cloudRes * _invertedTextureLength;
                stepLength = max(stepLength, STEP_SIZE);
                nextStep = rd * stepLength;
                p += nextStep;
                dS += 0.1 * clamp(shape(p) - 0.05, 0, 1);
                if (dS > 0.95) { break; }
                if (!insideSphere(center, radius, p))
                {
                    break;
                }
                texturePoint = getTexturePoint(p);

            }
            retVal = dS;
            return retVal;
        }
     
        float4 frag(v2f i) : SV_Target
        {
            float3 uv = i.uv;
            float3 ro = i.ro;
            float3 rd = normalize(i.hitPos - ro);
            
            if (!insideSphere(_center, _radius, ro))
            {
                ro = i.hitPos;
            }

            float3 rayPos = _WorldSpaceCameraPos;
            
            float4 result = rayMarch(ro, rd, _center, _radius);
            //float lighting = lightMarch(result.xyz, _WorldSpaceLightPos0, _boundsMin, _boundsMax);
            
            float4 col = float4(1.0, 1.0, 1.0, result.w);
            float3 wallColor = float3(1.0, 1.0, 1.0);
            if (result.x < 0)
                result.x *= 0.3;
            
            col.xyz = 0.5*(1.0+result.x) * _LightColor0.xyz;//lerp(wallColor, float3(1.0, 1.0, 1.0), result.w);
                col.w = result.w;
            return col;
        }

        ENDCG
        }
    }
    FallBack "Diffuse"
}
