﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorleyNoiseGenerator : MonoBehaviour
{
    public Texture2D WorleyNoise;
    private List<Vector2> WorleyPoints;
    private bool paramUpdated = true;

    [Range(0, 500)]
    public int _worleyPointsNumber = 15;
    private int _oldWorleyPointsNumber;

    public int _textureResolution = 128;
    private int _oldTextureResolution;

    [Range(0, 500)]
    public float _cloudSize = 50.0f;
    private float _oldCloudSize;

    public bool _invert = false;
    private bool _oldInvert;

    private Worley WorleyGenerator;

    public RenderTexture tempRenderTexture;
    public ComputeShader tempComputeShader;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        updateParams();
        if (paramUpdated)
        {
            WorleyGenerator = new Worley(_textureResolution,_worleyPointsNumber,_cloudSize);
            calculateTexture();
            GetComponent<RawImage>().texture = WorleyNoise;
            paramUpdated = false;
		}
    }

    private float calculateTexture()
    {
        WorleyNoise = new Texture2D(_textureResolution, _textureResolution);
        for (int cordX = 0; cordX < _textureResolution; cordX++)
            for (int cordY = 0; cordY < _textureResolution; cordY++)
            {
                float brightnessLevel = WorleyGenerator.Noise((float)cordX, (float)cordY);
                brightnessLevel = _invert ? (-1) * brightnessLevel + 1 : brightnessLevel;
                WorleyNoise.SetPixel(cordX, cordY, new Color(brightnessLevel, brightnessLevel, brightnessLevel));
                
            }
        WorleyNoise.Apply();
        GameObject.FindWithTag("Combined").GetComponent<CombinedNoiseGenerator>().worleyNoise = WorleyNoise;
        GameObject.FindWithTag("Combined").GetComponent<CombinedNoiseGenerator>().textureUpdated = true;

        tempRenderTexture = new RenderTexture(256, 256, 24);
        tempRenderTexture.enableRandomWrite = true;
        tempRenderTexture.Create();
        tempComputeShader.SetTexture(0,"Result", tempRenderTexture);
       // tempComputeShader.Dispatch(0, tempRenderTexture.width / 8, tempRenderTexture.height / 8, 1);


        return 0f;
    }

    private void updateParams()
    {
        if((_worleyPointsNumber != _oldWorleyPointsNumber) || (_textureResolution != _oldTextureResolution) || (_cloudSize != _oldCloudSize) || (_invert != _oldInvert))
        {
            paramUpdated = true;
        }
        _oldWorleyPointsNumber =_worleyPointsNumber;
        _oldTextureResolution = _textureResolution;
        _oldCloudSize = _cloudSize;
        _oldInvert = _invert;
    }

    private void prepareComputeBuffer()
    {

	}

    private void fillComputeBuffer()
    {

    }

    
}
