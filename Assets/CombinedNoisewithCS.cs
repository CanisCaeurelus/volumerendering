﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CombinedNoisewithCS : MonoBehaviour
{
    public Texture3D perlinNoise;
    public Texture3D worleyNoise;
    public RenderTexture combinedNoise3d;
    public Texture2D combinedNoise;
    public bool textureUpdated = false;
    int kernelLoadHandle;
    public ComputeShader shader;
    ComputeBuffer textureBuffer;

    public int _textureResolution=128;

    [Range(0.0f, 1.0f)]
    public float zLevel = 0;

    // Start is called before the first frame update
    void Start()
    {
/*
        combinedNoise3d = new RenderTexture(_textureResolution, _textureResolution, 0);
        combinedNoise3d.volumeDepth = _textureResolution;
        combinedNoise3d.dimension = UnityEngine.Rendering.TextureDimension.Tex3D;
        combinedNoise3d.enableRandomWrite = true;
        combinedNoise3d.Create();
*/
        combinedNoise = new Texture2D(_textureResolution, _textureResolution);
    }

    // Update is called once per frame
    void Update()
    {
        if (true == textureUpdated)
        {
            combinedNoise3d.enableRandomWrite = true;
            kernelLoadHandle = shader.FindKernel("CSNorm");
            shader.SetInt("texResolution", _textureResolution);
            shader.SetInt("zLevel", (int)(zLevel * (_textureResolution - 1)));

            shader.SetTexture(kernelLoadHandle, "Load", combinedNoise3d);
            shader.SetTexture(kernelLoadHandle, "Result", combinedNoise);

            shader.Dispatch(kernelLoadHandle, _textureResolution / 8, _textureResolution / 8, 1);

            combinedNoise.Apply();
            GetComponent<RawImage>().texture = combinedNoise;
            textureUpdated = false;
        }
    }
}
