﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PerlinWithCS : MonoBehaviour
{
    public ComputeShader shader;
    public RenderTexture texture;

    public int texResolution = 256;
    private int _oldTexResolution;

    [Range(1, 10)]
    public int octaves = 3;
    private int _oldOctaves;

    [Range(0.01f, 3.0f)]
    public float _frequency = 0.15f;
    private float _oldFrequency;

    [Range(0.0f, 1.0f)]
    public float zLevel = 0;
    private float oldZLevel;

    public bool _invert = false;
    private bool _oldInvert;

    bool paramUpdated = true;
    bool zLevelUpdated = false;

    int kernelHandle;
    int kernelNormHandle;
    int kernelClearHandle;
    int kernelSaveHandle;

    int dispatchXkernelCount;
    int dispatchYZkernelCount;

    private RenderTexture tex;

    ComputeBuffer textureBuffer;
    ComputeBuffer permBuffer;

    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        updateParams();
        if (paramUpdated)
        {
            UpdateShader();
            paramUpdated = false;
		}
        UpdateZLevel();
    }

    void OnDestroy()
    {
        textureBuffer.Release();
        permBuffer.Release();
    }

    private void UpdateShader()
    {
        if (textureBuffer != null)
            textureBuffer.Release();
        if (permBuffer != null)
            permBuffer.Release();
        
        texture = new RenderTexture(texResolution, texResolution, 0);
        texture.enableRandomWrite = true;
        texture.Create();

        GetComponent<RawImage>().texture = texture;
        textureBuffer = new ComputeBuffer(texResolution * texResolution * texResolution, 4 * sizeof(float));

        permBuffer = new ComputeBuffer(256, sizeof(int));
        permBuffer.SetData(perm,0,0,256);

        kernelHandle = shader.FindKernel("CSMain");
        kernelNormHandle = shader.FindKernel("CSNorm");

        shader.SetTexture(kernelHandle, "Result", texture);
        shader.SetBuffer(kernelHandle, "StructuredTexture", textureBuffer);
        shader.SetBuffer(kernelHandle, "permut", permBuffer);

        shader.SetInt("texResolution", texResolution);
        shader.SetInt("octaves", (int)(octaves));
        shader.SetFloat("frequency", _frequency);
        shader.SetInt("zLevel", (int)(zLevel * (texResolution-1)));

        shader.SetTexture(kernelNormHandle, "Result", texture);
        shader.SetBuffer(kernelNormHandle, "StructuredTexture", textureBuffer);

        shader.Dispatch(kernelHandle, texResolution / 8, texResolution / 8, texResolution / 8);
        shader.Dispatch(kernelNormHandle, texResolution / 8, texResolution / 8, 1);
    }
    private void updateParams()
    {
        if ((octaves != _oldOctaves) || (texResolution != _oldTexResolution) || (_frequency != _oldFrequency) || (_invert != _oldInvert))
        {
            paramUpdated = true;
        }
        _oldOctaves = octaves;
        _oldTexResolution = texResolution;
        _oldFrequency = _frequency;
        _oldInvert = _invert;
        if (zLevel != oldZLevel)
        {
            zLevelUpdated = true;
		}
        oldZLevel = zLevel;
    }

    void UpdateZLevel()
    {
        if (zLevelUpdated == true)
        {
            zLevelUpdated = false;
            shader.SetInt("zLevel", (int)(zLevel * (texResolution - 1)));
            shader.Dispatch(kernelNormHandle, texResolution / 8, texResolution / 8, 1);
        }

    }

    static int[] perm = {
        151,160,137,91,90,15,
        131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,
        190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,
        88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,134,139,48,27,166,
        77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,
        102,143,54, 65,25,63,161, 1,216,80,73,209,76,132,187,208, 89,18,169,200,196,
        135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,250,124,123,
        5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,
        223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 43,172,9,
        129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,97,228,
        251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,107,
        49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,
        138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180,
        151
    };

    [ContextMenu("Save texture")]
    public void saveTexture()
    {

        tex = new RenderTexture(texResolution, texResolution,0);
        tex.volumeDepth = texResolution;
        tex.enableRandomWrite = true;
        tex.dimension = UnityEngine.Rendering.TextureDimension.Tex3D;
        tex.Create();

        kernelSaveHandle = shader.FindKernel("CSSave");
        shader.SetTexture(kernelSaveHandle, "Save", tex);
        shader.SetBuffer(kernelSaveHandle, "StructuredTexture", textureBuffer);
        shader.Dispatch(kernelSaveHandle, texResolution / 8, texResolution / 8, texResolution / 8);

        GameObject.FindWithTag("Combined").GetComponent<CombinedNoisewithCS>().combinedNoise3d = tex;
        GameObject.FindWithTag("Combined").GetComponent<CombinedNoisewithCS>().textureUpdated = true;
    }

}
